# README #
## ***NO LONGER UPDATED*** ##
These are no longer being updated. We are no longer using detailed record widgets; the functions in this script are now part of the EDS-Complete repository.

## What this does ##

### Requesting ###
EDS allows us to make custom links for requesting item. However, we can't set those not to display when the item is not requestable. The script looks at the RTAC table and, if it sees a string indicating a circulating item, displays the link. The link opens in  a div created below the RTAC table rather than in a separate page. If it does not find any such string, it displays a note instead.

### Additional Links ###
The script also displays additional links below the RTAC table allowing the user to view the item in the OPAC or to execute a search in external catalogs.

### E-Reserves ###
In the case that the item is one of our small number of ereserves, the script calls for a PHP script, which scrapes the OPAC screen and displays the ereserve links instead of the request form.

## To Do ##
* Find out if different college profiles should have different additional links
* Work on wording of additional links button