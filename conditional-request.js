var college = window.parent.document.getElementById("collegeID").innerHTML;
function showExtLinks() {
  var oclc = 'ep.oclc';
  var isbn = 'ep.isbn';
  var wcBase = 'http://worldcat.org/';
  var zipFrag = '';
  switch (college) {
  case 'arc':
    zipFrag = '41';
    break;
  case 'crc':
    zipFrag = '23';
    break;
  case 'flc':
    zipFrag = '30';
    break;
  case 'scc':
    zipFrag = '22';
  }
  var zip = '&loc=958' + zipFrag;
  var title = window.parent.document.getElementsByTagName('h1')[0].innerHTML; // can't use EBSCO token because it may contain quotation marks/apostrophes
  var authorTitle = encodeURIComponent('ep.author ' + title); 
  authorTitle = authorTitle.replace('%2C', '');
  authorTitle = authorTitle.replace('%3A', '');
  var splLink = '<li><a target="_blank" id="splLink" onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'Sac Public\');" href="http://find.saclibrarycatalog.org/iii/encore/search?formids=target&lang=eng&suite=def&reservedids=lang%2Csuite&submitmode=&submitname=&target=' + authorTitle + '">Search for this item at Sacramento Public Library</a></li>';
  var wcText = 'Search for this item at CSUS, UC Davis and other libraries';
  var oclcLink = '';
  var oclcGA = 'onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'Worldcat\');"';
  if (oclc) {
    oclc = oclc.replace('ocm', '');
    oclcLink = '<li><a id="oclcLink" ' + oclcGA + ' target="_blank" href="' + wcBase + 'oclc/' + oclc + zip + '">' + wcText + '</a></li>';
  } else if (isbn) {
    oclcLink = '<li><a id="oclcLink" ' + oclcGA + ' target="_blank" href="' + wcBase + 'isbn/' + isbn + zip + '">' + wcText + '</a></li>';
  }
  var secondaryRequestMarkup = '<ul id="secondary-request">' + splLink + oclcLink + '</ul>';
  var secondaryLinks = window.parent.document.getElementById('secondaryLinks');
  if (window.parent.document.getElementById('secondaryLinksList') === null) {
    var secondaryLinksList = window.parent.document.createElement('div');
    secondaryLinksList.id = 'secondaryLinksList';
    secondaryLinksList.innerHTML = secondaryRequestMarkup;
    secondaryLinks.appendChild(secondaryLinksList);
    var secondaryLinksButton = window.parent.document.getElementById('secondaryLinksToggle');
    secondaryLinksButton.setAttribute('aria-pressed', 'true');
    secondaryLinksButton.textContent = '- Search other libraries';
    //    secondaryLinksButton.setAttribute('onclick', pathToFrame + '.hideExtLinks()');
    secondaryLinksButton.removeEventListener('click', showExtLinks);
    secondaryLinksButton.addEventListener('click', function(e) {
      e.preventDefault();
      hideExtLinks();
    });

  }
}

function showLoisLink(loisNo) {
  var loisURL = 'http://lois.losrios.edu/record=' + loisNo;
  var viewInLois = '<span class="lois-link"><a onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'LOIS\');" href="' + loisURL + '" target="_blank">View Catalog Record</a></span>';
  var dbDD = window.parent.document.querySelector('#citationFields dd:last-of-type');
  console.log(dbDD.innerHTML);
  var loisStatement = dbDD.innerHTML;
  dbDD.innerHTML = loisStatement + ' ' + viewInLois;
}

function hideExtLinks() {
  var secondaryLinks = window.parent.document.getElementById('secondaryLinks');
  secondaryLinks.removeChild(secondaryLinks.childNodes[1]);
  var secondaryLinksButton = window.parent.document.getElementById('secondaryLinksToggle');
  secondaryLinksButton.textContent = '+ Search other libraries';
  secondaryLinksButton.setAttribute('aria-pressed', 'false');
  secondaryLinksButton.removeEventListener('click', hideExtLinks);
  secondaryLinksButton.addEventListener('click', function(e) {
    e.preventDefault();
    showExtLinks();
  });

}

function openRequestPage(num, type) {
  // remove request button
  window.parent.document.getElementById('lr-conditional-request').style.display = 'none';
  // place iframe inside ebsco  page
  var holdingsArea = window.parent.document.querySelector('.record-has-rtac');
  var requestFrameContainer = window.parent.document.getElementById('lr-frame-container');
  requestURL = '';
  var height = '';
  var frameClass = '';
  var frameOffset = '';
  var frameParent = holdingsArea;
  if (type == 'request') {
    requestURL = 'https://lasiii.losrios.edu/search~S9?/.' + num + '/.' + num + '/1,1,1,B/request~' + num;
    height = '800';
    frameClass = 'lr-req';
    frameOffset = '-90';
  } else if (type == 'ereserve') {
    requestURL = 'https://www.library.losrios.edu/resources/onesearch/detailed-record/ereserves/view.php?college=' + college + '&an=' + num;
    height = '500';
    frameClass = 'lr-eres';
    frameOffset = '0';
    frameParent = window.parent.document.querySelector('.citation-wrapping-div');
  }
  var frameMarkup = '<div class="lr-frame-closer"><a id="req-newwin" href="' + requestURL + '" target="blank" onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'' + type + ' new window\');">Open this in a new window</a><button type="button" id="reqframe-closer" aria-label="close"> X </button></div><div id="request-container"><div id="request-scroll"><iframe class="' + frameClass + '" id="request-frame" width="100%" height="' + height + '" src="' + requestURL + '"></div></div></iframe>';
  if (requestFrameContainer === null) {
    requestFrameContainer = document.createElement('div');
    requestFrameContainer.id = 'lr-frame-container';
    requestFrameContainer.innerHTML = frameMarkup;
    frameParent.appendChild(requestFrameContainer);
  } else {
    requestFrameContainer.innerHTML = frameMarkup;
  }
  setTimeout(function() {
    window.parent.document.getElementById('reqframe-closer').addEventListener('click', function(e) {
      closeReqIframe();
      e.preventDefault();
      //            window.parent.document.getElementById('toolbarControl').scrollIntoView();
    });
    //       requestFrameContainer.scrollIntoView();
    window.parent.document.getElementById('request-frame').contentWindow.focus();
  }, 100);
}

function closeReqIframe() {
  holdContainer = window.parent.document.getElementById('lr-frame-container');
  holdContainer.parentNode.removeChild(holdContainer);
  window.parent.document.getElementById('lr-conditional-request').style.display = 'block';
}
var isCatRec = window.parent.document.querySelector('.record-has-rtac');
  (function() {
if (isCatRec) {

    // place request link under rtac table. Need to delay execution so that holdings load first.
    var rtacCall = setInterval(function() {
      if (window.parent.document.querySelector('.rtac-table') === null) {
        console.log('rtac nto found');
      } else {
        console.log('rtac found');
        clearInterval(rtacCall);
        // first get text from table
        var requestLinkSpace = window.parent.document.createElement('div');
        requestLinkSpace.id = 'lr-conditional-request';
        var rtacLocs = window.parent.document.querySelectorAll('.rtac-table tbody td:first-child');
        var accessNo = 'ep.uid';
        var loisNo = accessNo.replace('lrois.', '');
        showLoisLink(loisNo);
        var requestURL = 'https://lasiii.losrios.edu/search~S9?/.' + loisNo + '/.' + loisNo + '/1,1,1,B/request~' + loisNo;
        var secondaryLinksBut = '<div id="secondaryLinks"><button id="secondaryLinksToggle" type="button" onclick="ga(\'send\', \'event\', \'catalog record\', \'toggle\', \'secondary links\');">+ Search other libraries</button></div>';
        var rtacArea = window.parent.document.querySelector('.record-has-rtac');

        for (var i = 0; i < rtacLocs.length; i++) {
          // strings found in locations that circulate. Needs to be inclusive.
          var circItems = /Circulating|Juvenile|Children|Popular|SCC-3rd Floor|New Book|Display Books|Success|ARC-DVDs-1st Floor|(Inst((ruct)?)\.Med-1st Flr)/g;
          var eReserves = 'E-Reserves';
          // if at least one circulating item is found, request link shows
          if (rtacLocs[i].textContent.search(circItems) > -1) {
            //	alert('circulates! '+rtacLocs[i].textContent);
            requestLinkSpace.innerHTML = '<div id="request"><button type="button" id="request-button" class="button" onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'request\');"><img src="//www.library.losrios.edu/resources/link-icons/check-smallblack.png" alt=""> Request this item</button></div>' + secondaryLinksBut; // should embellish this
            rtacArea.appendChild(requestLinkSpace);

            break;
          } else if (rtacLocs[i].textContent.indexOf(eReserves) > -1) // e-reserves; can't live without 'em, can't kill 'em
          {
            requestLinkSpace.innerHTML = '<div id="request"><a onclick="ga(\'send\', \'event\', \'catalog record\', \'click\', \'ereserves\');" id="eres-request" href="' + requestURL + '">View eReserves</a></div><div id="secondary-request"><a href="http://lois.losrios.edu/record=' + loisNo + '" target="_blank">View in Classic Catalog (LOIS)</a></div>';
            rtacArea.appendChild(requestLinkSpace);
            var dts = window.parent.document.querySelectorAll('dt'); // ereserve records don't need these fields...
            for (i = 0; i < dts.length; i++) {
              if (dts[i].textContent.search(/Publication|Document/) > -1) {
                dts[i].setAttribute('class', 'hidden');
              }
            }
            var dds = window.parent.document.querySelectorAll('.hidden + dd');
            for (i = 0; i < dds.length; i++) {
              dds[i].setAttribute('class', 'hidden');
            }
            openRequestPage('ep.uid', 'ereserve');
          } else {
            requestLinkSpace.innerHTML = '<div id="request">No requestable copies found</div>' + secondaryLinksBut;
            rtacArea.appendChild(requestLinkSpace);
          }
        }
        var requestButton = window.parent.document.getElementById('request-button');
        if (requestButton) {

          requestButton.addEventListener('click', function(e) {
            e.preventDefault();
            openRequestPage(loisNo, 'request');
          });
        }
        var ereserveLink = window.parent.document.getElementById('eres-request');
        if (ereserveLink) {
          ereserveLink.addEventListener('click', function(e) {
            e.preventDefault();
            openRequestPage('ep.uid', 'ereserve', college);
          });
        }


        var secondLinks = window.parent.document.getElementById('secondaryLinksToggle');
        if (secondLinks) {
          secondLinks.addEventListener('click', function(e) {
            e.preventDefault();
            showExtLinks();
          });
        }

      }

    }, 200);
 
}
})();